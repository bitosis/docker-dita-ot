# Docker DITA-OT

DITA-OT Docker image.

## Supported tags

* [`latest`, `4.1.2`, `4.1.2-jre`, `4.1.2-jre17`](https://gitlab.com/bitosis/docker-dita-ot/container_registry/)
* [`4.1.2-jdk`, `4.1.2-jdk17`](https://gitlab.com/bitosis/docker-dita-ot/container_registry/)


## What is DITA Open Toolkit?

[DITA Open Toolkit][1] is an open-source publishing engine for content authored
in the Darwin Information Typing Architecture (DITA). While DITA was originally
developed to meet IBM's technical documentation requirements the DITA
architecture is completely general and can therefore be used productively for
any kind of document. DITA is used in a wide variety of industries and for a
wide variety of document types that go well beyond typical technical
documentation, including commercial Publishing, pharmaceutical information,
standards, training materials, and much more. Because DITA is inherently
extensible it can be adapted to almost any document requirement in a way that
maximizes the ability to use existing tools and knowledge.


## How to use this image

Open a shell for running `dita` commands:

```bash
docker run --rm -it registry.gitlab.com/bitosis/docker-dita-ot:latest bash
```

Or via `docker compose`:

```yaml
version: "3.8"
services:
  dita:
    image: registry.gitlab.com/bitosis/docker-dita-ot:latest
    volumes:
      - ./plugins:/docker-entrypoint-init.d
```

The above example utilizes the entrypoint directory to install additional
DITA-OT plugins (see [Initialization scripts](#initialization-scripts)).


## Initialization scripts

### Install DITA-OT plugins and fonts

If you would like to install DITA-OT plugins or fonts in an image derived from
this one, add one or more plugins (`*.zip` files), `*.ttf` files, `*.odt`
files, or `*.sh` scripts under `/docker-entrypoint-init.d` (creating the
directory if necessary). The entrypoint script will install any plugin or font
files and run any executable `*.sh` scripts found in that directory.


[1]: https://www.dita-ot.org/
