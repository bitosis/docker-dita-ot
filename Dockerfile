# syntax=docker/dockerfile:1

ARG ECLIPSE_TEMURIN_VERSION=17-jre

FROM eclipse-temurin:$ECLIPSE_TEMURIN_VERSION

RUN <<EOF
set -e
apt-get update --yes --quiet
apt-get install --yes --quiet --no-install-recommends curl fop unzip
rm -rf /var/lib/apt/lists/*
EOF

ARG DITA_OT_VERSION
ENV DITA_OT_VERSION=${DITA_OT_VERSION:-4.1.2}

WORKDIR /tmp

# Download and install DITA-OT
RUN <<EOF
set -e
curl -sLO "https://github.com/dita-ot/dita-ot/releases/download/$DITA_OT_VERSION/dita-ot-$DITA_OT_VERSION.zip"
unzip -qq "dita-ot-$DITA_OT_VERSION.zip" -d /opt
rm "dita-ot-$DITA_OT_VERSION.zip"
mv "/opt/dita-ot-$DITA_OT_VERSION" /opt/dita-ot
/opt/dita-ot/bin/dita --install
EOF

ENV PATH="${PATH}:/opt/dita-ot/bin"

RUN mkdir /docker-entrypoint-init.d

WORKDIR /root

COPY docker-entrypoint.sh /usr/local/bin/
ENTRYPOINT ["docker-entrypoint.sh"]

CMD ["/bin/bash"]
