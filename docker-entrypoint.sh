#!/usr/bin/env bash

set -Eeo pipefail

update_font_cache=0

docker_process_init_files() {
    local f
    for f; do
        case "$f" in
            *.sh)
                if [ -x "$f" ]; then
                    printf '%s: running %s\n' "$0" "$f"
                    "$f"
                fi
                ;;
            *.zip)     printf '%s: installing %s\n' "$0" "$f"; dita --force install "$f"; printf '\n' ;;
            *.ttf)     printf '%s: installing %s\n' "$0" "$f"; cp "$f" /usr/local/share/fonts; update_font_cache=1; printf '\n' ;;
            *.otf)     printf '%s: installing %s\n' "$0" "$f"; cp "$f" /usr/local/share/fonts; update_font_cache=1; printf '\n' ;;
            *)         printf '%s: ignoring %s\n' "$0" "$f" ;;
        esac
    done
}

_main() {
    docker_process_init_files /docker-entrypoint-init.d/*

    if [ $update_font_cache -eq 1 ]; then
        fc-cache
    fi

    echo "DITA-OT init process complete."

    exec "$@"
}

_main "$@"
